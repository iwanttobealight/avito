import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
// import reduxFreeze from 'redux-freeze';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import mainReducer from './src/store/reducers/main';
import coordsReducer from './src/store/reducers/coords';
import linksReducer from './src/store/reducers/links';
import errorReducer from './src/store/reducers/error';

import App from './src/containers/App';
import './src/styles/style.scss';

const appReducer = combineReducers({
  main: mainReducer,
  coords: coordsReducer,
  links: linksReducer,
  error: errorReducer
});

const store = createStore(
  appReducer,
  applyMiddleware(thunk)
);


render(<Provider store={store}><App /></Provider>, document.getElementById('app'));
