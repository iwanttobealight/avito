export const photoProps = {
  DEFAULT_NUM: 0,
  INTERVAL_CHANGE: 5000,
  INTERVAL_STEP: 100,
  INTERVAL_ANIMATION: 500
};

export const rangeProps = {
  STEP: 500,
  MIN: 0,
  MAX: 0
};

export const sortTypes = {
  POPULAR: 'popular',
  CHEAP_FIRST: 'cheap-first',
  EXPENSIVE_FIRST: 'expensive-first'
};

export const categoryTypes = {
  DEFAULT: 'all',
  AUTO: 'auto',
  IMMOVABLE: 'immovable',
  LAPTOPS: 'laptops',
  CAMERAS: 'cameras'
};

export const productsProps = {
  ID: 'id',
  ADDRESS: 'address',
  PRICE: 'price',
  YEAR: 'year',
  BODY_TYPE: 'body_type',
  GEARBOX: 'gearbox',
  PROPERTY_TYPE: 'property_type',
  ROOMS: 'rooms',
  SQUARE: 'square',
  LAPTOP_TYPE: 'laptop_type',
  RAM: 'ram',
  SCREEN: 'screen',
  PROCESSOR: 'processor',
  CAMERA_TYPE: 'camera_type',
  MATRIX_RESOLUTION: 'matrix_resolution',
  VIDEO_RESOLUTION: 'video_resolution',
};

export const googleMap = {
  REQUEST_LINK: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=',
  MAP_LINK: 'https://www.google.ru/maps/@',
  KEY: 'AIzaSyBCJONE9hW8oVrSxeH2oxWLzVZvFjOxUvU',
  CARD_ZOOM: 13,
  MAP_ZOOM: 16
};

export const LOCAL_STORAGE_VAR = 'products';

export const localStorage = {
  PRODUCTS: 'products',
  COORDS: 'coordinates',
  DETAILS_COORDS: 'detailsCoordinates',
  REQUESTS_NUM: 'requestsNum'
};

export const FIRST_TAB_INDEX = 1;

export const STEP = 1;

export const DEFAULT_VALUE = false;

export const sellerRating = {
  GOOD: 4.8,
  REGULAR: 4
};

