import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://avito.dump.academy'
});

export default instance;
