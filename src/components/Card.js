import React, { Component } from 'react';
import PropTypes from 'prop-types';
import nanoId from 'nanoid';
import GoogleMapReact from 'google-map-react';

import * as appData from '../app-data';

import Button from '../components/UI/Button';
import Marker from '../components/Marker';

class Card extends Component {
  state = {
    zoom: appData.googleMap.CARD_ZOOM,
    sellerInfo: {},
    sellerRaitingClasses: '',
    pictures: [],
    map: null
  }

  openMapInNewWindow() {
    window.open(`${appData.googleMap.MAP_LINK}${this.center.lat},${this.center.lng},${appData.googleMap.MAP_ZOOM}z`, "_blank");
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.productsInfo.id !== this.props.productsInfo.id) {
      let sellerRaitingClass;
      const sellerRating = nextProps.sellersInfo[nextProps.productsInfo.sellerId].rating;

      if (sellerRating > appData.sellerRating.GOOD) {
        sellerRaitingClass = 'details-seller-rating-good';
      } else if (sellerRating > appData.sellerRating.REGULAR) {
        sellerRaitingClass = 'details-seller-rating-average';
      } else if (sellerRating < appData.sellerRating.REGULAR) {
        sellerRaitingClass = 'details-seller-rating-bad';
      }

      const sellerRaitingClasses = 'details-seller-rating'.concat(` ${sellerRaitingClass}`);

      this.setState({ sellerInfo: nextProps.sellersInfo[nextProps.productsInfo.sellerId], sellerRaitingClasses });
    }

    if (nextProps.productsInfo.pictures) {
      const detailsPictures = nextProps.productsInfo.pictures.slice(1);
      const pictures = detailsPictures.map(pic => {
        return <img key={nanoId()} src={pic} alt={nextProps.productsInfo.title} className="details-gallery-item" />;
      });
      this.setState({ pictures });
    }

    if (nextProps.coords[nextProps.productsInfo.id]) {
      const map =
        <GoogleMapReact
          bootstrapURLKeys={{
            language: 'ru',
            region: 'ru',
          }}
          onClick={this.openMapInNewWindow}
          defaultCenter={{ lat: 0, lng: 0 }}
          center={nextProps.coords[nextProps.productsInfo.id]}
          defaultZoom={this.state.zoom}
        >
          <Marker
            lat={nextProps.coords[nextProps.productsInfo.id].lat}
            lng={nextProps.coords[nextProps.productsInfo.id].lng}
          />
        </GoogleMapReact>;
      this.setState({ map });
    }
  }

  render() {
    return (
      <article className="details" >
        <Button className="details-close" onButtonClick={this.props.closePopup}>Закрыть</Button>
        <h2 className="details-title">{this.props.productsInfo.title}</h2>
        <section className="details-main">
          <p className="details-date" />
          <p className="details-price">{this.props.productsInfo.price}&nbsp;₽</p>
          <section className="details-gallery">
            <img src={this.props.productsInfo.pictures ? this.props.productsInfo.pictures[0] : null} alt={this.props.productsInfo.title} className="details-gallery-preview" />
            <div className="details-gallery-thumbnails">
              {this.state.pictures}
            </div>
          </section>

          <p className="details-description" />
        </section>

        <aside className="details-aside">
          <section className="details-address">
            <p className="details-address-text">{this.props.productsInfo.address}</p>
            <div className="details-address-map">
              {this.state.map}
            </div>
          </section>
          <section className="details-seller">
            <a href="#" className="details-seller-link">
              <h3 className="details-seller-name">{this.state.sellerInfo.name}</h3>
            </a>

            <p className={this.state.sellerRaitingClasses}>
              рейтинг
              <span className="details-seller-rating-val">{this.state.sellerInfo.rating}</span>
              <a href="#">Отзывы</a>
            </p>
          </section>
        </aside>
      </article>
    );
  }
}

Card.propTypes = {
  closePopup: PropTypes.func.isRequired,
  coords: PropTypes.array.isRequired,
  productsInfo: PropTypes.object.isRequired,
  sellersInfo: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
};

export default Card;
