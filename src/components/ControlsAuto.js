import React from 'react';
import PropTypes from 'prop-types';

import nanoId from 'nanoid';
import Select from './UI/Select';


const ControlsAuto = (props) => (
  <React.Fragment>
    <Select
      value={props.currentAutoYear}
      onSelectChange={props.filterAutoYear}
      name="auto-year"
      id="auto-year"
    >
      <option value="all">Минимальный год выпуска</option>
      {props.autoYears.map((year) => <option key={nanoId()} value={year}>{year}</option>)}
    </Select>
    <Select
      value={props.currentAutoTransmission}
      onSelectChange={props.filterAutoTransmission}
      name="auto-transmission"
      id="auto-transmission"
    >
      <option value="all">Тип коробки передач</option>
      {props.autoTransmissionTypes.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
    <Select
      value={props.currentAutoBodyType}
      onSelectChange={props.filterAutoBodyType}
      name="auto-body-type"
      id="auto-body-type"
    >
      <option value="all">Тип кузова</option>
      {props.autoBodyTypes.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
  </React.Fragment >
);

ControlsAuto.propTypes = {
  autoYears: PropTypes.array.isRequired,
  autoTransmissionTypes: PropTypes.array.isRequired,
  autoBodyTypes: PropTypes.array.isRequired,
  currentAutoYear: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentAutoTransmission: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentAutoBodyType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  filterAutoYear: PropTypes.func.isRequired,
  filterAutoTransmission: PropTypes.func.isRequired,
  filterAutoBodyType: PropTypes.func.isRequired,
};

export default ControlsAuto;
