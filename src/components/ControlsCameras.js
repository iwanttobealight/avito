import React from 'react';
import PropTypes from 'prop-types';

import nanoId from 'nanoid';
import Select from './UI/Select';


const ControlsCameras = (props) => (
  <React.Fragment>
    <Select
      value={props.currentCamerasType}
      onSelectChange={props.filterCamerasTypes}
      name="cameras-type"
      id="cameras-type"
    >
      <option value="all">Тип</option>
      {props.camerasTypes.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
    <Select
      value={props.currentCamerasMatrix}
      onSelectChange={props.filterCamerasMatrix}
      name="cameras-matrix"
      id="cameras-matrix"
    >
      <option value="all">Разрешение матрицы</option>
      {props.camerasMatrix.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
    <Select
      value={props.currentCamerasVideo}
      onSelectChange={props.filterCamerasVideo}
      name="cameras-video"
      id="cameras-video"
    >
      <option value="all">Разрешение видео</option>
      {props.camerasVideo.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
  </React.Fragment>);

ControlsCameras.propTypes = {
  currentCamerasType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentCamerasMatrix: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentCamerasVideo: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  camerasTypes: PropTypes.array.isRequired,
  camerasMatrix: PropTypes.array.isRequired,
  camerasVideo: PropTypes.array.isRequired,
  filterCamerasTypes: PropTypes.func.isRequired,
  filterCamerasMatrix: PropTypes.func.isRequired,
  filterCamerasVideo: PropTypes.func.isRequired,
};

export default ControlsCameras;
