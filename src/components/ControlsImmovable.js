import React from 'react';
import PropTypes from 'prop-types';

import nanoId from 'nanoid';
import Select from './UI/Select';


const ControlsImmovable = (props) => (
  <React.Fragment>
    <Select
      value={props.currentImmovableType}
      onSelectChange={props.filterImmovableType}
      name="immovable-type"
      id="immovable-type"
    >
      <option value="all">Тип</option>
      {props.immovableTypes.map((type) => <option key={nanoId()} value={type}>{type}</option>)}
    </Select>
    <Select
      value={props.currentImmovableArea}
      onSelectChange={props.filterImmovableArea}
      name="immovable-area"
      id="immovable-area"
    >
      <option value="all">Минимальная площадь</option>
      {props.immovableAreas.map((area) => <option key={nanoId()} value={area}>{area}</option>)}
    </Select>
    <Select
      value={props.currentImmovableRooms}
      onSelectChange={props.filterImmovableRooms}
      name="immovable-rooms"
      id="immovable-rooms"
    >
      <option value="all">Количество комнат</option>
      {props.immovableRooms.map((room) => <option key={nanoId()} value={room}>{room}</option>)}
    </Select>
  </React.Fragment>);

ControlsImmovable.propTypes = {
  filterImmovableType: PropTypes.func.isRequired,
  filterImmovableArea: PropTypes.func.isRequired,
  filterImmovableRooms: PropTypes.func.isRequired,
  immovableTypes: PropTypes.array.isRequired,
  immovableAreas: PropTypes.array.isRequired,
  immovableRooms: PropTypes.array.isRequired,
  currentImmovableType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentImmovableArea: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentImmovableRooms: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
};

export default ControlsImmovable;
