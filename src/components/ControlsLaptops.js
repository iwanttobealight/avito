import React from 'react';
import PropTypes from 'prop-types';

import nanoId from 'nanoid';
import Select from './UI/Select';


const ControlsLaptops = (props) => (
  <React.Fragment>
    <Select
      value={props.currentLaptopsType}
      onSelectChange={props.filterLaptopsTypes}
      name="laptops-type"
      id="laptops-type"
    >
      <option value="all">Тип</option>
      {props.laptopsTypes.map((type) => <option key={nanoId()} value={type}>{type}</option>)}

    </Select>
    <Select
      value={props.currentLaptopsRAM}
      onSelectChange={props.filterLaptopsRAMs}
      name="laptops-RAM"
      id="laptops-RAM"
    >
      <option value="all">Минимальный объем RAM</option>
      {props.laptopsRAMs.map((item) => <option key={nanoId()} value={item}>{item}</option>)}
    </Select>
    <Select
      value={props.currentLaptopsScreen}
      onSelectChange={props.filterLaptopsScreens}
      name="laptops-screen"
      id="laptops-screen"
    >
      <option value="all">Минимальная диагональ экрана</option>
      {props.laptopsScreens.map((screen) => <option key={nanoId()} value={screen}>{screen}</option>)}
    </Select>
    <Select
      value={props.currentLaptopsCPU}
      onSelectChange={props.filterLaptopsCPUs}
      name="laptops-CPU"
      id="laptops-CPU"
    >
      <option value="all">Тип процессора</option>
      {props.laptopsCPUs.map((item) => <option key={nanoId()} value={item}>{item}</option>)}
    </Select>
  </React.Fragment>);

ControlsLaptops.propTypes = {
  currentLaptopsType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsRAM: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsScreen: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsCPU: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  filterLaptopsTypes: PropTypes.func.isRequired,
  filterLaptopsRAMs: PropTypes.func.isRequired,
  filterLaptopsScreens: PropTypes.func.isRequired,
  filterLaptopsCPUs: PropTypes.func.isRequired,
  laptopsTypes: PropTypes.array.isRequired,
  laptopsRAMs: PropTypes.array.isRequired,
  laptopsScreens: PropTypes.array.isRequired,
  laptopsCPUs: PropTypes.array.isRequired,
};

export default ControlsLaptops;
