import React from 'react';
import PropTypes from 'prop-types';

const Marker = props => (
  <div
    className="marker"
    lat={props.lat}
    lng={props.lng}
  >
    <img height="40px" width="24px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Map_marker.svg/390px-Map_marker.svg.png" />
  </div>);


Marker.propTypes = {
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired
};

export default Marker;
