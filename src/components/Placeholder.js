import React from 'react';
import PropTypes from 'prop-types';

const Placeholder = props => (
  <article className={props.placeholderVisible ? "products-list-item product" : "hidden"}>
    <h3 className="product-title">Нет товаров по данному запросу</h3>
  </article>
);

Placeholder.propTypes = {
  placeholderVisible: PropTypes.bool.isRequired
};

export default Placeholder;
