import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { retainUniqueElements, formatPrice } from '../store/utils';

import * as actionTypes from '../store/actions/actionTypes';
import * as appData from '../app-data';

import Button from './UI/Button';

class Product extends Component {
  state = {
    photoNum: appData.photoProps.DEFAULT_NUM,
    photoAnimation: '',
    interval: appData.photoProps.INTERVAL_CHANGE + appData.photoProps.INTERVAL_STEP * this.props.id,
    favoriteProductsIds: [],
    tabIndex: +(this.props.tabIndex) == '0' ? appData.FIRST_TAB_INDEX : +(this.props.tabIndex) + appData.STEP,
    productsInfo: {
      id: this.props.id,
      sellerId: this.props.sellerId,
      title: this.props.title,
      price: formatPrice(this.props.price),
      pictures: this.props.pictures,
      address: this.props.detailsCoordinates[this.props.id]
    },
  }

  componentDidMount() {
    this.showPhoto();
    this.deleteAnimation();
    this.showFavoritesIcons(this.props.favoriteProducts);
  }

  componentWillReceiveProps(nextProps) {
    this.showFavoritesIcons(nextProps.favoriteProducts);
  }

  showPhoto() {
    setInterval(() => {
      let newPhotoNum;

      if (this.state.photoNum === this.props.pictures.length - appData.STEP) {
        newPhotoNum = appData.photoProps.DEFAULT_NUM;
      } else {
        newPhotoNum = this.state.photoNum + appData.STEP;
      }
      this.setState({ photoNum: newPhotoNum, photoAnimation: 'fade' });
    }, this.state.interval);
  }

  deleteAnimation() {
    setInterval(() => {
      this.setState({ photoAnimation: '' });
    }, this.state.interval + appData.photoProps.INTERVAL_ANIMATION);
  }

  showFavoritesIcons(favoriteProducts) {
    let favProductsIds = [];
    for (const prop in favoriteProducts) {
      for (const key in favoriteProducts[prop]) {
        if (key === appData.productsProps.ID) {
          favProductsIds.push(favoriteProducts[prop][key]);
          if (favProductsIds.indexOf(this.props.id) !== -1) {
            favProductsIds = favProductsIds.filter(item => item.id !== this.props.id);
          }
        }
      }
    }

    this.setState({ favoriteProductsIds: favProductsIds });
  }

  onEnterKeyPress(e) {
    if (e.key === 'Enter') {
      this.props.showPopup(this.state.productsInfo);
    }
  }

  render() {
    return (
      <article className="products-list-item product">
        <picture className="product-pic">
          <a role="status" tabIndex="-1" className="product-pic-number">{this.props.pictures.length}</a>
          <img
            className={this.state.photoAnimation}
            src={this.props.pictures[this.state.photoNum]}
            width="120"
            alt={this.props.title}
          />
        </picture>
        <div className="product-description">
          <Button
            id={this.props.id}
            className={
              retainUniqueElements(this.state.favoriteProductsIds).indexOf(this.props.id) !== -1 ? "product-favorite product-favorite-check" : "product-favorite"}
            text="Добавить в избранное"
            type="button"
            tabIndex={this.state.tabIndex + appData.STEP}
            onButtonClick={(e) => this.props.saveToLocalStorage(e)}
          />
          <h3 className="product-title">
            <a tabIndex={this.state.tabIndex} role="heading" onKeyPress={(e) => this.onEnterKeyPress(e)} onClick={() => this.props.showPopup(this.state.productsInfo)}>{this.props.title}</a>
          </h3>
          <p className="product-price">{formatPrice(this.props.price)} ₽</p>
          <p className="product-address">{this.props.address}</p>
          <p className="product-date" />
        </div>
      </article>
    );
  }
}


Product.propTypes = {
  pictures: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  address: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  sellerId: PropTypes.string.isRequired,
  saveToLocalStorage: PropTypes.func.isRequired,
  coordinates: PropTypes.array.isRequired,
  favoriteProducts: PropTypes.array.isRequired,
  showPopup: PropTypes.func.isRequired,
  detailsCoordinates: PropTypes.array.isRequired,
  tabIndex: PropTypes.number.isRequired
};

const mapStateToProps = state => {
  return {
    coordinates: state.coords.coordinates,
    detailsCoordinates: state.coords.detailsCoordinates,
    favoriteProducts: state.main.favoriteProducts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToLocalStorage: (e) => dispatch({ type: actionTypes.SAVE_TO_LOCAL, e }),
    showPopup: (info) => dispatch({ type: actionTypes.SHOW_POPUP, info })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
