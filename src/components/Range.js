import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { formatPrice } from '../store/utils';

import * as actionTypes from '../store/actions/actionTypes';
import * as appData from '../app-data';

const Range = props => (
  <React.Fragment>
    <span className="price-range-min">{formatPrice(props.rangeMin)}&nbsp;₽</span>
    <input type="range" name="price-range" id="price-range" min={props.rangeMin} max={props.rangeMax} step={appData.rangeProps.STEP} onChange={props.setMaxPrice} disabled={props.disabled} />
    <output className="price-range-max" htmlFor="price-range">{formatPrice(props.rangeCurrentNum)}&nbsp;₽</output>
  </React.Fragment>
);

Range.propTypes = {
  rangeMin: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
  rangeMax: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
  rangeCurrentNum: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
  setMaxPrice: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired
};

Range.defaultProps = {
  rangeMin: 'min',
  rangeMax: 'max',
  rangeCurrentNum: 'max',
};

const mapStateToProps = state => {
  return {
    rangeMin: state.main.rangeMin,
    rangeMax: state.main.rangeMax,
    rangeCurrentNum: state.main.rangeCurrentNum,
    disabled: state.main.isRangeDisabled
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setMaxPrice: (e) => dispatch({ type: actionTypes.SET_MAX_PRICE, e })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Range);

