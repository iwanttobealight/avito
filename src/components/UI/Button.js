import React from 'react';
import PropTypes from 'prop-types';

const Button = props => (
  <button
    type={props.type}
    className={props.className}
    onClick={props.onButtonClick}
    disabled={props.disabled}
    tabIndex={props.tabIndex}
    id={props.id}
  >
    {props.text}
  </button>);

Button.propTypes = {
  onButtonClick: PropTypes.func.isRequired,
  type: PropTypes.string,
  className: PropTypes.string,
  text: PropTypes.string,
  tabIndex: PropTypes.any,
  disabled: PropTypes.bool,
  id: PropTypes.string
};

Button.defaultProps = {
  className: '',
  type: 'submit',
  text: '',
  disabled: false,
  tabIndex: '',
  id: ''
};

export default Button;
