import React from 'react';
import PropTypes from 'prop-types';


const Checkbox = props => {
  const onEnterKeyPress = (e) => {
    props.onChange(e);
  };

  return (
    <React.Fragment>
      <input
        type="checkbox"
        name={props.name}
        id={props.id}
        checked={props.checked}
        onChange={props.onChange}
        onKeyPress={(e) => onEnterKeyPress(e)}
        disabled={props.disabled}
      />
      <label className="radiogroup-item" htmlFor={props.id}>{props.label}</label>
    </React.Fragment>
  );
};

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
};

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
};

export default Checkbox;
