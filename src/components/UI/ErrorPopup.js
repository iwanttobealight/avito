import React from 'react';
import PropTypes from 'prop-types';

import Button from '../UI/Button';

const ErrorPopup = props => (
  props.visible ? <div className="error"><p>{`error: ${props.errorMessage}`}</p><Button className="error-button" text="close" onButtonClick={props.closePopup} /></div> : null
);

ErrorPopup.propTypes = {
  visible: PropTypes.bool.isRequired,
  closePopup: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
};

ErrorPopup.defaultProps = {
  errorMessage: 'unhandled error (sad story)'
};

export default ErrorPopup;
