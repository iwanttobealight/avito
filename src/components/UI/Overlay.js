import React from 'react';
import PropTypes from 'prop-types';
import CloseOnEscape from 'react-close-on-escape';


const Overlay = props => <CloseOnEscape onEscape={props.closePopup}><div className={props.isOverlayShow ? "overlay" : "overlay hidden"}>{props.children}</div></CloseOnEscape>;

Overlay.propTypes = {
  children: PropTypes.any.isRequired,
  closePopup: PropTypes.func.isRequired,
  isOverlayShow: PropTypes.bool,
};

Overlay.defaultProps = {
  isOverlayShow: false
};

export default Overlay;
