import React from 'react';
import PropTypes from 'prop-types';

const RadioButton = props => (
  <React.Fragment>
    <input
      type="radio"
      name={props.name}
      value={props.value}
      id={props.id}
      className="radio-button"
      checked={props.checked}
      onChange={props.onChange}
      disabled={props.disabled}
    />
    <label className="radiogroup-item" htmlFor={props.id}>{props.label}</label>
  </React.Fragment>
);

RadioButton.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool
};

RadioButton.defaultProps = {
  checked: false,
  disabled: false
};

export default RadioButton;
