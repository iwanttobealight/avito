import React from 'react';
import PropTypes from 'prop-types';

const Select = props => (
  <select
    value={props.value}
    onChange={props.onSelectChange}
    name={props.name}
    id={props.id}
    disabled={props.disabled}
  >
    {props.children}
  </select>);


Select.propTypes = {
  value: PropTypes.any.isRequired,
  onSelectChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  children: PropTypes.any.isRequired,
};

Select.defaultProps = {
  name: 'select',
  id: 'default',
  disabled: false
};

export default Select;
