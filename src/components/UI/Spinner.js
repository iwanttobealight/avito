import React from 'react';

const Spinner = () => <img className="loader" height="600" src="https://cdn.dribbble.com/users/93144/screenshots/2348873/loader.mp4.gif" />;

export default Spinner;
