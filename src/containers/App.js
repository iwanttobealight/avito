import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as actionTypes from '../store/actions/actionTypes';
import * as actions from '../store/actions/index';
import * as appData from '../app-data';

import Card from '../components/Card';
import Overlay from '../components/UI/Overlay';
import ErrorPopup from '../components/UI/ErrorPopup';
import ProductList from './ProductsList';
import ProductsFilter from './ProductsFilter';
import Loader from '../components/Loader';
import Placeholder from '../components/Placeholder';

class App extends Component {
  state = {
    isCoordsLoaded: false,
  }

  componentDidMount() {
    this.props.getLinks();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.products !== this.props.products) {
      nextProps.getProducts(nextProps.products);
      nextProps.getSellers(nextProps.sellers);
    }
    if (nextProps.requestsNum >= nextProps.allProducts.length) {
      this.setState({ isCoordsLoaded: true });
    } else if (this.props.coords !== nextProps.coords) {
      const firstCoordsArr = nextProps.coords.slice(0, 50);
      const secondCoordsArr = nextProps.coords.slice(50, 100);
      for (const coord of firstCoordsArr) {
        const addressLink = `${appData.googleMap.REQUEST_LINK}${coord.lat},${coord.lng}&key=${appData.googleMap.KEY}`;
        this.props.geoCode(addressLink);
      }
      setTimeout(() => {
        for (const coord of secondCoordsArr) {
          const addressLink = `${appData.googleMap.REQUEST_LINK}${coord.lat},${coord.lng}&key=${appData.googleMap.KEY}`;
          this.props.geoCode(addressLink);
        }
      }, 1000);
    }
  }
  render() {
    return (
      <React.Fragment>
        <main className="layout centered">
          <section className="layout-main products-list">
            <Loader isLoading={this.state.isCoordsLoaded}>
              <ProductList screenProducts={this.props.screenProducts} coordinates={this.props.coordinates} />
            </Loader>
            <Placeholder
              placeholderVisible={this.props.placeholderVisible}
            />
          </section>
          <aside className="layout-sidebar">
            <ProductsFilter onButtonClick={this.props.onFilterButtonClick} />
          </aside>
        </main>
        <Overlay isOverlayShow={this.props.isOverlayShow} closePopup={this.props.closePopup}>
          <Card
            screenProducts={this.props.screenProducts}
            coords={this.props.coords}
            isOverlayShow={this.props.isOverlayShow}
            productsInfo={this.props.productsInfo}
            sellersInfo={this.props.sellersInfo}
            closePopup={this.props.closePopup}
          />
        </Overlay>
        <ErrorPopup
          visible={this.props.visibleError}
          errorMessage={this.props.errorMessage}
          closePopup={this.props.closeErrorPopup}
        />
      </React.Fragment>);
  }
}

App.propTypes = {
  getLinks: PropTypes.func.isRequired,
  getProducts: PropTypes.func.isRequired,
  getSellers: PropTypes.func.isRequired,
  geoCode: PropTypes.func.isRequired,
  products: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  onFilterButtonClick: PropTypes.func.isRequired,
  isOverlayShow: PropTypes.bool.isRequired,
  closePopup: PropTypes.func.isRequired,
  closeErrorPopup: PropTypes.func.isRequired,
  visibleError: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  sellers: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  requestsNum: PropTypes.number.isRequired,
  coords: PropTypes.array.isRequired,
  screenProducts: PropTypes.array.isRequired,
  allProducts: PropTypes.array.isRequired,
  placeholderVisible: PropTypes.bool.isRequired,
  productsInfo: PropTypes.object.isRequired,
  sellersInfo: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  coordinates: PropTypes.array.isRequired
};

const mapStateToProps = state => {
  return {
    products: state.links.products,
    sellers: state.links.sellers,
    coords: state.main.coords,
    coordinates: state.coords.coordinates,
    allProducts: state.main.allProducts,
    isOverlayShow: state.main.isOverlayShow,
    visibleError: state.error.visibleError,
    errorMessage: state.error.errorMessage,
    requestsNum: state.coords.requestsNum,
    screenProducts: state.main.screenProducts,
    currentFilter: state.main.currentFilter,
    placeholderVisible: state.main.placeholderVisible,
    currentProducts: state.main.currentProducts,
    productsInfo: state.main.productsInfo,
    sellersInfo: state.main.sellersInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getLinks: () => dispatch(actions.getLinks()),
    getProducts: (address) => dispatch(actions.getProducts(address)),
    getSellers: (address) => dispatch(actions.getSellers(address)),
    geoCode: (address) => dispatch(actions.geoCode(address)),
    onFilterButtonClick: () => dispatch({ type: actionTypes.FILTER_BUTTON_CLICK }),
    closePopup: () => dispatch({ type: actionTypes.CLOSE_POPUP }),
    closeErrorPopup: () => dispatch({ type: actionTypes.CLOSE_ERROR_POPUP }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
