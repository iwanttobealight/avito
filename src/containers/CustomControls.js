import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as appData from '../app-data';
import * as actionTypes from '../store/actions/actionTypes';

import RadioButton from '../components/UI/RadioButton';
import ControlsAuto from '../components/ControlsAuto';
import ControlsImmovable from '../components/ControlsImmovable';
import ControlsLaptops from '../components/ControlsLaptops';
import ControlsCameras from '../components/ControlsCameras';

const CustomControls = props => {
  let customControls = null;
  switch (props.category) {
    case appData.categoryTypes.DEFAULT:
      customControls = null;
      break;
    case appData.categoryTypes.AUTO:
      customControls =
        <ControlsAuto
          autoYears={props.autoYears}
          autoTransmissionTypes={props.autoTransmissionTypes}
          autoBodyTypes={props.autoBodyTypes}
          currentAutoYear={props.currentAutoYear}
          currentAutoTransmission={props.currentAutoTransmission}
          currentAutoBodyType={props.currentAutoBodyType}
          filterAutoYear={props.filterAutoYear}
          filterAutoTransmission={props.filterAutoTransmission}
          filterAutoBodyType={props.filterAutoBodyType}
        />;
      break;
    case appData.categoryTypes.IMMOVABLE:
      customControls =
        <ControlsImmovable
          filterImmovableType={props.filterImmovableType}
          filterImmovableArea={props.filterImmovableArea}
          filterImmovableRooms={props.filterImmovableRooms}
          immovableTypes={props.immovableTypes}
          immovableAreas={props.immovableAreas}
          immovableRooms={props.immovableRooms}
          currentImmovableType={props.currentImmovableType}
          currentImmovableArea={props.currentImmovableArea}
          currentImmovableRooms={props.currentImmovableRooms}
        />;
      break;
    case appData.categoryTypes.LAPTOPS:
      customControls =
        <ControlsLaptops
          currentLaptopsType={props.currentLaptopsType}
          currentLaptopsRAM={props.currentLaptopsRAM}
          currentLaptopsScreen={props.currentLaptopsScreen}
          currentLaptopsCPU={props.currentLaptopsCPU}
          filterLaptopsTypes={props.filterLaptopsTypes}
          filterLaptopsRAMs={props.filterLaptopsRAMs}
          filterLaptopsScreens={props.filterLaptopsScreens}
          filterLaptopsCPUs={props.filterLaptopsCPUs}
          laptopsTypes={props.laptopsTypes}
          laptopsRAMs={props.laptopsRAMs}
          laptopsScreens={props.laptopsScreens}
          laptopsCPUs={props.laptopsCPUs}
        />;
      break;
    case appData.categoryTypes.CAMERAS:
      customControls =
        <ControlsCameras
          currentCamerasType={props.currentCamerasType}
          currentCamerasMatrix={props.currentCamerasMatrix}
          currentCamerasVideo={props.currentCamerasVideo}
          camerasTypes={props.camerasTypes}
          camerasMatrix={props.camerasMatrix}
          camerasVideo={props.camerasVideo}
          filterCamerasTypes={props.filterCamerasTypes}
          filterCamerasMatrix={props.filterCamerasMatrix}
          filterCamerasVideo={props.filterCamerasVideo}
        />;
      break;
    default:
      customControls = null;
  }
  return (
    <React.Fragment>
      <RadioButton name="sort" value={appData.sortTypes.POPULAR} id="sort-popular" label="популярные" checked={props.checkedPopular} disabled={props.isRadioButtonsDisabled} onChange={props.sortProductsCost} />
      <RadioButton name="sort" value={appData.sortTypes.CHEAP_FIRST} id="sort-cheap" label="дешевые" checked={props.checkedCheap} disabled={props.isRadioButtonsDisabled} onChange={props.sortProductsCost} />
      <RadioButton name="sort" value={appData.sortTypes.EXPENSIVE_FIRST} id="sort-expensive" label="дорогие" checked={props.checkedExpensive} disabled={props.isRadioButtonsDisabled} onChange={props.sortProductsCost} />
      {customControls}
    </React.Fragment>
  );
};


CustomControls.propTypes = {
  sortProductsCost: PropTypes.func.isRequired,
  category: PropTypes.string.isRequired,
  filterAutoYear: PropTypes.func.isRequired,
  filterAutoTransmission: PropTypes.func.isRequired,
  filterAutoBodyType: PropTypes.func.isRequired,
  autoYears: PropTypes.array.isRequired,
  autoBodyTypes: PropTypes.array.isRequired,
  autoTransmissionTypes: PropTypes.array.isRequired,
  currentAutoYear: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentAutoTransmission: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentAutoBodyType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  filterImmovableType: PropTypes.func.isRequired,
  filterImmovableArea: PropTypes.func.isRequired,
  filterImmovableRooms: PropTypes.func.isRequired,
  currentImmovableType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentImmovableArea: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentImmovableRooms: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  immovableTypes: PropTypes.array.isRequired,
  immovableAreas: PropTypes.array.isRequired,
  immovableRooms: PropTypes.array.isRequired,
  currentLaptopsType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsRAM: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsScreen: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentLaptopsCPU: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  filterLaptopsTypes: PropTypes.func.isRequired,
  filterLaptopsRAMs: PropTypes.func.isRequired,
  filterLaptopsScreens: PropTypes.func.isRequired,
  filterLaptopsCPUs: PropTypes.func.isRequired,
  laptopsTypes: PropTypes.array.isRequired,
  laptopsRAMs: PropTypes.array.isRequired,
  laptopsScreens: PropTypes.array.isRequired,
  laptopsCPUs: PropTypes.array.isRequired,
  currentCamerasType: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentCamerasMatrix: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  currentCamerasVideo: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  camerasTypes: PropTypes.array.isRequired,
  camerasMatrix: PropTypes.array.isRequired,
  camerasVideo: PropTypes.array.isRequired,
  filterCamerasTypes: PropTypes.func.isRequired,
  filterCamerasMatrix: PropTypes.func.isRequired,
  filterCamerasVideo: PropTypes.func.isRequired,
  isRadioButtonsDisabled: PropTypes.bool.isRequired,
  checkedPopular: PropTypes.bool,
  checkedCheap: PropTypes.bool,
  checkedExpensive: PropTypes.bool,
};

CustomControls.defaultProps = {
  checkedPopular: false,
  checkedCheap: false,
  checkedExpensive: false,
};

const mapStateToProps = state => {
  return {
    category: state.main.category,
    checkedPopular: state.main.checkedPopular,
    checkedCheap: state.main.checkedCheap,
    checkedExpensive: state.main.checkedExpensive,
    autoYears: state.main.autoYears,
    autoBodyTypes: state.main.autoBodyTypes,
    autoTransmissionTypes: state.main.autoTransmissionTypes,
    currentAutoYear: state.main.currentAutoYear,
    currentAutoBodyType: state.main.currentAutoBodyType,
    currentAutoTransmission: state.main.currentAutoTransmission,
    immovableTypes: state.main.immovableTypes,
    immovableAreas: state.main.immovableAreas,
    immovableRooms: state.main.immovableRooms,
    currentImmovableType: state.main.currentImmovableType,
    currentImmovableArea: state.main.currentImmovableArea,
    currentImmovableRooms: state.main.currentImmovableRooms,
    currentLaptopsType: state.main.currentLaptopsType,
    currentLaptopsRAM: state.main.currentLaptopsRAM,
    currentLaptopsScreen: state.main.currentLaptopsScreen,
    currentLaptopsCPU: state.main.currentLaptopsCPU,
    filterLaptopsTypes: state.main.filterLaptopsTypes,
    filterLaptopsRAMs: state.main.filterLaptopsRAMs,
    filterLaptopsScreens: state.main.filterLaptopsScreens,
    filterLaptopsCPUs: state.main.filterLaptopsCPUs,
    laptopsTypes: state.main.laptopsTypes,
    laptopsRAMs: state.main.laptopsRAMs,
    laptopsScreens: state.main.laptopsScreens,
    laptopsCPUs: state.main.laptopsCPUs,
    currentCamerasType: state.main.currentCamerasType,
    currentCamerasMatrix: state.main.currentCamerasMatrix,
    currentCamerasVideo: state.main.currentCamerasVideo,
    filterCamerasTypes: state.main.filterCamerasTypes,
    filterCamerasMatrix: state.main.filterCamerasMatrix,
    filterCamerasVideo: state.main.filterCamerasVideo,
    camerasTypes: state.main.camerasTypes,
    camerasMatrix: state.main.camerasMatrix,
    camerasVideo: state.main.camerasVideo,
    isRadioButtonsDisabled: state.main.isRadioButtonsDisabled
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sortProductsCost: (e) => dispatch({ type: actionTypes.SORT_STANDART, e }),
    filterAutoYear: (e) => dispatch({ type: actionTypes.FILTER_AUTO_YEAR, e }),
    filterAutoTransmission: (e) => dispatch({ type: actionTypes.FILTER_AUTO_TRANSMISSION, e }),
    filterAutoBodyType: (e) => dispatch({ type: actionTypes.FILTER_AUTO_BODY_TYPE, e }),
    filterImmovableType: (e) => dispatch({ type: actionTypes.FILTER_IMMOVABLE_TYPE, e }),
    filterImmovableArea: (e) => dispatch({ type: actionTypes.FILTER_IMMOVABLE_AREA, e }),
    filterImmovableRooms: (e) => dispatch({ type: actionTypes.FILTER_IMMOVABLE_ROOMS, e }),
    filterLaptopsTypes: (e) => dispatch({ type: actionTypes.FILTER_LAPTOPS_TYPES, e }),
    filterLaptopsRAMs: (e) => dispatch({ type: actionTypes.FILTER_LAPTOPS_RAMS, e }),
    filterLaptopsScreens: (e) => dispatch({ type: actionTypes.FILTER_LAPTOPS_SCREENS, e }),
    filterLaptopsCPUs: (e) => dispatch({ type: actionTypes.FILTER_LAPTOPS_CPUS, e }),
    filterCamerasTypes: (e) => dispatch({ type: actionTypes.FILTER_CAMERAS_TYPES, e }),
    filterCamerasMatrix: (e) => dispatch({ type: actionTypes.FILTER_CAMERAS_MATRIX, e }),
    filterCamerasVideo: (e) => dispatch({ type: actionTypes.FILTER_CAMERAS_VIDEO, e }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomControls);
