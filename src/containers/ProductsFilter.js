import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as actionTypes from '../store/actions/actionTypes';

import Checkbox from '../components/UI/Checkbox';
import Select from '../components/UI/Select';
import Button from '../components/UI/Button';
import CustomControls from '../containers/CustomControls';
import Range from '../components/Range';

const ProductsFilter = props => (
  <form className="products-filter">
    <fieldset className="products-filter-group radiogroup products-filter-favorite">
      <Checkbox
        name="is-favorite"
        id="favorite"
        checked={props.checkedFavorite}
        onChange={props.sortFavorite}
        label="Показывать избранные"
      />
    </fieldset>

    <fieldset className="products-filter-group">
      <label htmlFor="category">Категория</label>
      <br />
      <Select
        id="category"
        name="category"
        value={props.category}
        onSelectChange={props.filterCategories}
        disabled={props.isGeneralSelectDisabled}
      >
        <option value="all">Все объявления</option>
        <option value="auto">Авто</option>
        <option value="immovable">Недвижимость</option>
        <option value="laptops">Ноутбуки</option>
        <option value="cameras">Фотоаппараты</option>
      </Select>
    </fieldset >

    <fieldset className="products-filter-group radiogroup">
      Сначала:
      <CustomControls />
    </fieldset>

    <fieldset className="products-filter-group">
      <label htmlFor="price-range">Максимальная цена</label>
      <br />
      <Range />
    </fieldset>

    <Button onButtonClick={props.onButtonClick} className="products-filter-submit" type="button" text="Показать" disabled={props.isFilterButtonDisabled} />
  </form >);


ProductsFilter.propTypes = {
  onButtonClick: PropTypes.func.isRequired,
  sortFavorite: PropTypes.func.isRequired,
  checkedFavorite: PropTypes.bool.isRequired,
  isFilterButtonDisabled: PropTypes.bool.isRequired,
  category: PropTypes.string.isRequired,
  filterCategories: PropTypes.func.isRequired,
  isGeneralSelectDisabled: PropTypes.bool.isRequired,
};

const mapStateToProps = state => {
  return {
    category: state.main.category,
    isGeneralSelectDisabled: state.main.isGeneralSelectDisabled,
    checkedFavorite: state.main.checkedFavorite,
    isFilterButtonDisabled: state.main.isFilterButtonDisabled
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sortFavorite: (e) => dispatch({ type: actionTypes.SORT_FAV, e }),
    filterCategories: (e) => dispatch({ type: actionTypes.FILTER_CATS, e }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductsFilter);
