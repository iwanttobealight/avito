import React from 'react';
import PropTypes from 'prop-types';

import Product from '../components/Product';

const ProductList = (props) => {
  return (
    props.screenProducts.map((product, i) =>
      <Product
        key={product.id}
        id={product.id}
        tabIndex={i}
        pictures={product.pictures}
        title={product.title}
        price={product.price}
        address={props.coordinates[product.id]}
        sellerId={product.relationships.seller}
      />)
  );
};

ProductList.propTypes = {
  screenProducts: PropTypes.array.isRequired,
  coordinates: PropTypes.array.isRequired
};

export default ProductList;
