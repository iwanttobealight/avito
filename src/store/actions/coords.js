import * as actionTypes from './actionTypes';
import axios from '../../axios-data';

export const showErrorPopup = (message) => {
  return {
    type: actionTypes.SHOW_ERROR_POPUP,
    message
  };
};

export const initCoords = coords => {
  return {
    type: actionTypes.GEO_CODE,
    coords
  };
};

export const geoCode = (address) => {
  return (dispatch) => {
    axios.get(address)
      .then((response) => {
        if (response.data.error_message) {
          dispatch(showErrorPopup(response.data.error_message));
        }
        dispatch(initCoords(response.data.results));
      }, (error) => {
        dispatch(showErrorPopup(error.message));
      });
  };
};
