import * as actionTypes from './actionTypes';
import axios from '../../axios-data';

export const showErrorPopup = (message) => {
  return {
    type: actionTypes.SHOW_ERROR_POPUP,
    message
  };
};

export const initLinks = links => {
  return {
    type: actionTypes.INIT_LINKS,
    product: links.product,
    products: links.products,
    seller: links.seller,
    sellers: links.sellers
  };
};

export const getLinks = () => {
  return (dispatch) => {
    axios.get('https://avito.dump.academy')
      .then((response) => {
        dispatch(initLinks(response.data.links));
      }, (error) => {
        dispatch(showErrorPopup(error.message));
      });
  };
};
