import * as actionTypes from './actionTypes';
import axios from '../../axios-data';

export const showErrorPopup = (message) => {
  return {
    type: actionTypes.SHOW_ERROR_POPUP,
    message
  };
};

export const initProducts = data => {
  return {
    type: actionTypes.GET_PRODUCTS,
    data
  };
};

export const initSellers = data => {
  return {
    type: actionTypes.GET_SELLERS,
    data
  };
};

export const getProducts = (address) => {
  return (dispatch) => {
    axios.get(address)
      .then((response) => {
        dispatch(initProducts(response.data.data));
      })
      .catch((error) => {
        dispatch(showErrorPopup(error.message));
      });
  };
};

export const getSellers = (address) => {
  return (dispatch) => {
    axios.get(address)
      .then((response) => {
        dispatch(initSellers(response.data.data));
      })
      .catch((error) => {
        dispatch(showErrorPopup(error.message));
      });
  };
};

