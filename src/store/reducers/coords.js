import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utils';

import * as appData from '../../app-data';

const initialState = {
  coordinates: JSON.parse(localStorage.getItem(appData.localStorage.COORDS)) || [],
  detailsCoordinates: JSON.parse(localStorage.getItem(appData.localStorage.DETAILS_COORDS)) || [],
  requestsNum: JSON.parse(localStorage.getItem(appData.localStorage.REQUESTS_NUM)) || 0
};

const initCoordinates = (state, action) => {
  let coordinates = state.coordinates;
  let detailsCoordinates = state.detailsCoordinates;
  let requestsNum = state.requestsNum;

  let newCoord = `${action.coords[0].address_components[1].long_name}, ${action.coords[0].address_components[3].long_name}`;
  let newDetailCoord = `${action.coords[0].formatted_address}`;

  if (action.coords[0].address_components[0].long_name === 'Unnamed Road') {
    newCoord = `${action.coords[0].address_components[3].long_name}`;
  }

  if (action.coords[0].address_components[1].long_name === 'Р39') {
    newCoord = `${action.coords[0].address_components[2].long_name}, ${action.coords[0].address_components[4].long_name}`;
  }

  if (action.coords[0].formatted_address.split(',')[0] === 'Unnamed Road') {
    newDetailCoord = action.coords[0].formatted_address.split(',').slice(1).join(',');
  }

  coordinates = coordinates.concat(newCoord);
  detailsCoordinates = detailsCoordinates.concat(newDetailCoord);
  requestsNum += 1;

  localStorage.removeItem(appData.localStorage.COORDS);
  localStorage.removeItem(appData.localStorage.DETAILS_COORDS);
  localStorage.removeItem(appData.localStorage.REQUESTS_NUM);

  localStorage.setItem(appData.localStorage.COORDS, JSON.stringify(coordinates));
  localStorage.setItem(appData.localStorage.DETAILS_COORDS, JSON.stringify(detailsCoordinates));
  localStorage.setItem(appData.localStorage.REQUESTS_NUM, JSON.stringify(requestsNum));

  return updateObject(state, {
    coordinates,
    detailsCoordinates,
    requestsNum,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GEO_CODE: return initCoordinates(state, action);
    default: return state;
  }
};

export default reducer;
