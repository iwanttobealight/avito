import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utils';

const initialState = {
  visibleError: false,
  errorMessage: ''
};

const showErrorPopup = (state, action) => {
  return updateObject(state, {
    visibleError: true,
    errorMessage: action.message.toLowerCase()
  });
};


const closeErrorPopup = (state, action) => {
  return updateObject(state, {
    visibleError: false,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_ERROR_POPUP: return showErrorPopup(state, action);
    case actionTypes.CLOSE_ERROR_POPUP: return closeErrorPopup(state, action);
    default: return state;
  }
};

export default reducer;
