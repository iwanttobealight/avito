import * as actionTypes from '../actions/actionTypes';
import * as appData from '../../app-data';
import { updateObject } from '../utils';

const initialState = {
  product: appData.DEFAULT_VALUE,
  seller: appData.DEFAULT_VALUE,
  products: appData.DEFAULT_VALUE,
  sellers: appData.DEFAULT_VALUE,
};

const initLinks = (state, action) => {
  return updateObject(state, {
    product: action.product.replace(/^http:\/\//i, 'https://'),
    seller: action.seller.replace(/^http:\/\//i, 'https://'),
    products: action.products.replace(/^http:\/\//i, 'https://'),
    sellers: action.sellers.replace(/^http:\/\//i, 'https://'),
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INIT_LINKS: return initLinks(state, action);
    default: return state;
  }
};

export default reducer;
