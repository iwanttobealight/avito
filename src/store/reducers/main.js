import * as actionTypes from '../actions/actionTypes';
import * as appData from '../../app-data';
import { updateObject, sortMin, retainUniqueElements, getArrayKeysFromArrayObjects, retainUniqueElementsFromObjectsArray, customSort } from '../utils';

const initialState = {
  checkedPopular: true,
  checkedCheap: false,
  checkedExpensive: false,
  checkedFavorite: false,
  currentSort: appData.sortTypes.POPULAR,
  allProducts: [],
  productsWithoutPrices: [],
  currentProducts: [],
  screenProducts: [],
  categoryProducts: [],
  currentPopularProducts: [],
  coordinates: appData.DEFAULT_VALUE,
  category: appData.categoryTypes.DEFAULT,
  coords: [],
  autoYears: [],
  autoBodyTypes: [],
  autoTransmissionTypes: [],
  currentAutoYear: appData.DEFAULT_VALUE,
  currentAutoTransmission: appData.DEFAULT_VALUE,
  currentAutoBodyType: appData.DEFAULT_VALUE,
  immovableTypes: [],
  immovableAreas: [],
  immovableRooms: [],
  currentImmovableType: appData.DEFAULT_VALUE,
  currentImmovableArea: appData.DEFAULT_VALUE,
  currentImmovableRooms: appData.DEFAULT_VALUE,
  laptopsTypes: [],
  laptopsRAMs: [],
  laptopsScreens: [],
  laptopsCPUs: [],
  currentLaptopsType: appData.DEFAULT_VALUE,
  currentLaptopsRAM: appData.DEFAULT_VALUE,
  currentLaptopsScreen: appData.DEFAULT_VALUE,
  currentLaptopsCPU: appData.DEFAULT_VALUE,
  camerasTypes: [],
  camerasMatrix: [],
  camerasVideo: [],
  currentCamerasType: appData.DEFAULT_VALUE,
  currentCamerasMatrix: appData.DEFAULT_VALUE,
  currentCamerasVideo: appData.DEFAULT_VALUE,
  favoriteProducts: JSON.parse(localStorage.getItem(appData.localStorage.PRODUCTS)) || [],
  rangeMin: appData.rangeProps.MIN,
  rangeMax: appData.rangeProps.MAX,
  rangeCurrentNum: appData.DEFAULT_VALUE,
  placeholderVisible: false,
  isGeneralSelectDisabled: true,
  isRadioButtonsDisabled: true,
  isRangeDisabled: true,
  isFilterButtonDisabled: true,
  isOverlayShow: false,
  productsInfo: {},
  sellersInfo: {},
  currentHistoryState: '/',
  customControlsArrays: {
    autoYears: [],
    autoBodyTypes: [],
    autoTransmissionTypes: [],
    immovableTypes: [],
    immovableAreas: [],
    immovableRooms: [],
    laptopsTypes: [],
    laptopsRAMs: [],
    laptopsScreens: [],
    laptopsCPUs: [],
    camerasTypes: [],
    camerasMatrix: [],
    camerasVideo: [],
  },
  currentFilter: {
    category: '',
  }
};

const initProducts = (state, action) => {
  const coords = [];
  const productsWithUndefinedPrices = [];
  const productsWithPrice = [];
  let rangeMax = state.rangeMax;
  let rangeMin = state.rangeMin;

  const autoBodyTypes = [];
  const autoYears = [];
  const autoTransmissionTypes = [];
  const immovableTypes = [];
  const immovableRooms = [];
  const immovableAreas = [];
  const laptopsTypes = [];
  const laptopsRAMs = [];
  const laptopsScreens = [];
  const laptopsCPUs = [];
  const camerasTypes = [];
  const camerasMatrix = [];
  const camerasVideo = [];

  const customControlsArrays = {
    autoYears: [],
    autoBodyTypes: [],
    autoTransmissionTypes: [],
    immovableTypes: [],
    immovableAreas: [],
    immovableRooms: [],
    laptopsTypes: [],
    laptopsRAMs: [],
    laptopsScreens: [],
    laptopsCPUs: [],
    camerasTypes: [],
    camerasMatrix: [],
    camerasVideo: [],
  };

  for (const product of action.data) {
    for (const key in product) {
      if (key === appData.productsProps.ADDRESS) {
        coords.push(product[key]);
      }
      if (key === appData.productsProps.YEAR) {
        autoYears.push(product[key]);
        customControlsArrays.autoYears.push(product);
      }
      if (key === appData.productsProps.BODY_TYPE) {
        autoBodyTypes.push(product[key]);
        customControlsArrays.autoBodyTypes.push(product);
      }
      if (key === appData.productsProps.GEARBOX) {
        autoTransmissionTypes.push(product[key]);
        customControlsArrays.autoTransmissionTypes.push(product);
      }
      if (key === appData.productsProps.PROPERTY_TYPE) {
        immovableTypes.push(product[key]);
        customControlsArrays.immovableTypes.push(product);
      }
      if (key === appData.productsProps.ROOMS) {
        immovableRooms.push(product[key]);
        customControlsArrays.immovableRooms.push(product);
      }
      if (key === appData.productsProps.SQUARE) {
        immovableAreas.push(product[key]);
        customControlsArrays.immovableAreas.push(product);
      }
      if (key === appData.productsProps.LAPTOP_TYPE) {
        laptopsTypes.push(product[key]);
        customControlsArrays.laptopsTypes.push(product);
      }
      if (key === appData.productsProps.RAM) {
        laptopsRAMs.push(product[key]);
        customControlsArrays.laptopsRAMs.push(product);
      }
      if (key === appData.productsProps.SCREEN) {
        laptopsScreens.push(product[key]);
        customControlsArrays.laptopsScreens.push(product);
      }
      if (key === appData.productsProps.PROCESSOR) {
        laptopsCPUs.push(product[key]);
        customControlsArrays.laptopsCPUs.push(product);
      }
      if (key === appData.productsProps.CAMERA_TYPE) {
        camerasTypes.push(product[key]);
        customControlsArrays.camerasTypes.push(product);
      }
      if (key === appData.productsProps.MATRIX_RESOLUTION) {
        camerasMatrix.push(product[key]);
        customControlsArrays.camerasMatrix.push(product);
      }
      if (key === appData.productsProps.VIDEO_RESOLUTION) {
        camerasVideo.push(product[key]);
        customControlsArrays.camerasVideo.push(product);
      }
      if (product.price === undefined) {
        product.price = 0;
        productsWithUndefinedPrices.push(product);
      }
      if (product.price !== 0) {
        productsWithPrice.push(product);
      }
    }
  }

  const uniqueProductsWithPrice = retainUniqueElementsFromObjectsArray(productsWithPrice);
  const uniqueProductsWithUndefinedPrices = retainUniqueElementsFromObjectsArray(productsWithUndefinedPrices);

  const allProducts = [...uniqueProductsWithPrice, ...uniqueProductsWithUndefinedPrices];
  const prices = getArrayKeysFromArrayObjects(action.data, appData.productsProps.PRICE);

  rangeMin = prices.sort(sortMin)[0];
  rangeMax = prices.sort(sortMin)[prices.length - 1];

  return updateObject(state, {
    allProducts,
    productsWithoutPrices: uniqueProductsWithUndefinedPrices,
    screenProducts: allProducts,
    categoryProducts: allProducts,
    currentPopularProducts: allProducts,
    currentProducts: allProducts,
    coords,
    autoYears: retainUniqueElements(autoYears).sort(sortMin),
    autoBodyTypes: retainUniqueElements(autoBodyTypes),
    autoTransmissionTypes: retainUniqueElements(autoTransmissionTypes),
    immovableTypes: retainUniqueElements(immovableTypes),
    immovableAreas: retainUniqueElements(immovableAreas).sort(sortMin),
    immovableRooms: retainUniqueElements(immovableRooms).sort(sortMin),
    laptopsTypes: retainUniqueElements(laptopsTypes),
    laptopsRAMs: retainUniqueElements(laptopsRAMs).sort(sortMin),
    laptopsScreens: retainUniqueElements(laptopsScreens).sort(sortMin),
    laptopsCPUs: retainUniqueElements(laptopsCPUs),
    camerasTypes: retainUniqueElements(camerasTypes),
    camerasMatrix: retainUniqueElements(camerasMatrix).sort(sortMin),
    camerasVideo: retainUniqueElements(camerasVideo),
    rangeMin,
    rangeMax,
    rangeCurrentNum: prices.sort(sortMin)[prices.length - 1],
    isGeneralSelectDisabled: false,
    isRadioButtonsDisabled: false,
    isRangeDisabled: false,
    isFilterButtonDisabled: false
  });
};

const initSellers = (state, action) => {
  return updateObject(state, {
    sellersInfo: action.data
  });
};

const setCategory = (state, action) => {
  let newProducts = state.allProducts.filter(item => (item.category === action.e.target.value));
  let rangeMin = '';
  let rangeMax = '';

  if (action.e.target.value === appData.categoryTypes.DEFAULT) {
    newProducts = state.allProducts;
  }

  const prices = getArrayKeysFromArrayObjects(newProducts, appData.productsProps.PRICE);

  rangeMin = prices.sort(sortMin)[0];
  rangeMax = prices.sort(sortMin)[prices.length - 1];

  return updateObject(state, {
    currentFilter: `category:${action.e.target.value}`,
    category: action.e.target.value,
    currentProducts: newProducts,
    categoryProducts: newProducts,
    currentPopularProducts: newProducts,
    rangeMin,
    rangeMax,
    rangeCurrentNum: prices.sort(sortMin)[prices.length - 1],
    currentAutoYear: appData.DEFAULT_VALUE,
    currentAutoTransmission: appData.DEFAULT_VALUE,
    currentAutoBodyType: appData.DEFAULT_VALUE,
    currentImmovableType: appData.DEFAULT_VALUE,
    currentImmovableArea: appData.DEFAULT_VALUE,
    currentImmovableRooms: appData.DEFAULT_VALUE,
    currentLaptopsType: appData.DEFAULT_VALUE,
    currentLaptopsRAM: appData.DEFAULT_VALUE,
    currentLaptopsScreen: appData.DEFAULT_VALUE,
    currentLaptopsCPU: appData.DEFAULT_VALUE,
    currentCamerasType: appData.DEFAULT_VALUE,
    currentCamerasMatrix: appData.DEFAULT_VALUE,
    currentCamerasVideo: appData.DEFAULT_VALUE,
  });
};

const sortProductsCost = (state, action) => {
  let checkedPopular = false;
  let checkedCheap = false;
  let checkedExpensive = false;

  switch (action.e.target.id) {
    case 'sort-popular':
      checkedPopular = true;
      break;
    case 'sort-cheap':
      checkedCheap = true;
      break;
    case 'sort-expensive':
      checkedExpensive = true;
      break;
    default:
      break;
  }

  return updateObject(state, {
    currentSort: action.e.target.value,
    checkedPopular,
    checkedCheap,
    checkedExpensive
  });
};


const filterAutoYear = (state, action) => {
  return updateObject(state, {
    currentAutoYear: action.e.target.value
  });
};

const filterAutoTransmission = (state, action) => {
  let currentAutoTransmission;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentAutoTransmission = appData.DEFAULT_VALUE : currentAutoTransmission = action.e.target.value;
  return updateObject(state, {
    currentAutoTransmission
  });
};

const filterAutoBodyType = (state, action) => {
  let currentAutoBodyType;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentAutoBodyType = appData.DEFAULT_VALUE : currentAutoBodyType = action.e.target.value;
  return updateObject(state, {
    currentAutoBodyType
  });
};

const filterImmovableType = (state, action) => {
  let currentImmovableType;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentImmovableType = appData.DEFAULT_VALUE : currentImmovableType = action.e.target.value;
  return updateObject(state, {
    currentImmovableType
  });
};

const filterImmovableArea = (state, action) => {
  let currentImmovableArea;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentImmovableArea = appData.DEFAULT_VALUE : currentImmovableArea = action.e.target.value;
  return updateObject(state, {
    currentImmovableArea
  });
};

const filterImmovableRooms = (state, action) => {
  let currentImmovableRooms;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentImmovableRooms = appData.DEFAULT_VALUE : currentImmovableRooms = action.e.target.value;
  return updateObject(state, {
    currentImmovableRooms
  });
};

const filterLaptopsTypes = (state, action) => {
  let currentLaptopsType;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentLaptopsType = appData.DEFAULT_VALUE : currentLaptopsType = action.e.target.value;
  return updateObject(state, {
    currentLaptopsType
  });
};

const filterLaptopsRAMs = (state, action) => {
  let currentLaptopsRAM;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentLaptopsRAM = appData.DEFAULT_VALUE : currentLaptopsRAM = action.e.target.value;
  return updateObject(state, {
    currentLaptopsRAM
  });
};

const filterLaptopsScreens = (state, action) => {
  let currentLaptopsScreen;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentLaptopsScreen = appData.DEFAULT_VALUE : currentLaptopsScreen = action.e.target.value;
  return updateObject(state, {
    currentLaptopsScreen
  });
};

const filterLaptopsCPUs = (state, action) => {
  let currentLaptopsCPU;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentLaptopsCPU = appData.DEFAULT_VALUE : currentLaptopsCPU = action.e.target.value;
  return updateObject(state, {
    currentLaptopsCPU
  });
};

const filterCamerasTypes = (state, action) => {
  let currentCamerasType;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentCamerasType = appData.DEFAULT_VALUE : currentCamerasType = action.e.target.value;
  return updateObject(state, {
    currentCamerasType
  });
};

const filterCamerasMatrix = (state, action) => {
  let currentCamerasMatrix;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentCamerasMatrix = appData.DEFAULT_VALUE : currentCamerasMatrix = action.e.target.value;
  return updateObject(state, {
    currentCamerasMatrix
  });
};

const filterCamerasVideo = (state, action) => {
  let currentCamerasVideo;
  action.e.target.value === appData.categoryTypes.DEFAULT ? currentCamerasVideo = appData.DEFAULT_VALUE : currentCamerasVideo = action.e.target.value;
  return updateObject(state, {
    currentCamerasVideo
  });
};

const filterButtonClick = (state, action) => {
  let sortedProducts = [...state.currentProducts];
  let curHistoryState = '';
  let rangeMin = '';
  let rangeMax = state.rangeMax;
  const rangeCurrentNum = state.rangeCurrentNum;
  const productsWithZeroPrice = [];

  let placeholderVisible = false;
  switch (state.currentSort) {
    case appData.sortTypes.POPULAR:
      sortedProducts = state.currentPopularProducts;
      curHistoryState = curHistoryState.concat(appData.sortTypes.POPULAR);
      break;
    case appData.sortTypes.CHEAP_FIRST:
      sortedProducts = sortedProducts.sort((first, second) => {
        if (+(first.price) < +(second.price)) {
          return -1;
        } else if (+(first.price) > +(second.price)) {
          return 1;
        } else {
          return 0;
        }
      });
      curHistoryState = curHistoryState.concat(appData.sortTypes.CHEAP_FIRST);
      break;
    case appData.sortTypes.EXPENSIVE_FIRST:
      sortedProducts = sortedProducts.sort((first, second) => {
        if (+(first.price) > +(second.price)) {
          return -1;
        } else if (+(first.price) < +(second.price)) {
          return 1;
        } else {
          return 0;
        }
      });
      curHistoryState = curHistoryState.concat(appData.sortTypes.EXPENSIVE_FIRST);
      break;
    default:
      break;
  }


  // autos
  if (state.currentAutoTransmission !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.GEARBOX, state.currentAutoTransmission, sortedProducts, state.customControlsArrays.autoTransmissionTypes, curHistoryState, () => sortedProducts.filter(auto => auto.gearbox === state.currentAutoTransmission)).generalArray;
    curHistoryState = customSort(appData.productsProps.GEARBOX, state.currentAutoTransmission, sortedProducts, state.customControlsArrays.autoTransmissionTypes, curHistoryState, () => sortedProducts.filter(auto => auto.gearbox === state.currentAutoTransmission)).historyValue;
  }
  if (state.currentAutoBodyType !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.BODY_TYPE, state.currentAutoBodyType, sortedProducts, state.customControlsArrays.autoBodyTypes, curHistoryState, () => sortedProducts.filter(auto => auto.body_type === state.currentAutoBodyType)).generalArray;
    curHistoryState = customSort(appData.productsProps.BODY_TYPE, state.currentAutoBodyType, sortedProducts, state.customControlsArrays.autoBodyTypes, curHistoryState, () => sortedProducts.filter(auto => auto.body_type === state.currentAutoBodyType)).historyValue;
  }

  if (state.currentAutoYear !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.YEAR, state.currentAutoYear, sortedProducts, state.customControlsArrays.autoYears, curHistoryState, () => sortedProducts.filter(item => (+(item.year) >= +(state.currentAutoYear)))).generalArray;
    curHistoryState = customSort(appData.productsProps.YEAR, state.currentAutoYear, sortedProducts, state.customControlsArrays.autoYears, curHistoryState, () => sortedProducts.filter(item => (+(item.year) >= +(state.currentAutoYear)))).historyValue;
  }

  // immovable
  if (state.currentImmovableType !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.PROPERTY_TYPE, state.currentImmovableType, sortedProducts, state.customControlsArrays.immovableTypes, curHistoryState, () => sortedProducts.filter(item => item.property_type === state.currentImmovableType)).generalArray;
    curHistoryState = customSort(appData.productsProps.PROPERTY_TYPE, state.currentImmovableType, sortedProducts, state.customControlsArrays.immovableTypes, curHistoryState, () => sortedProducts.filter(item => item.property_type === state.currentImmovableType)).historyValue;
  }
  if (state.currentImmovableArea !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.SQUARE, state.currentImmovableArea, sortedProducts, state.customControlsArrays.immovableAreas, curHistoryState, () => sortedProducts.filter(item => +(item.square) >= +(state.currentImmovableArea))).generalArray;
    curHistoryState = customSort(appData.productsProps.SQUARE, state.currentImmovableArea, sortedProducts, state.customControlsArrays.immovableAreas, curHistoryState, () => sortedProducts.filter(item => +(item.square) >= +(state.currentImmovableArea))).historyValue;
  }

  if (state.currentImmovableRooms !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.ROOMS, state.currentImmovableRooms, sortedProducts, state.customControlsArrays.immovableRooms, curHistoryState, () => sortedProducts.filter(item => +(item.rooms) === +(state.currentImmovableRooms))).generalArray;
    curHistoryState = customSort(appData.productsProps.ROOMS, state.currentImmovableRooms, sortedProducts, state.customControlsArrays.immovableRooms, curHistoryState, () => sortedProducts.filter(item => +(item.rooms) === +(state.currentImmovableRooms))).historyValue;
  }

  // laptops
  if (state.currentLaptopsType !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.LAPTOP_TYPE, state.currentLaptopsType, sortedProducts, state.customControlsArrays.laptopsTypes, curHistoryState, () => sortedProducts.filter(item => item.laptop_type === state.currentLaptopsType)).generalArray;
    curHistoryState = customSort(appData.productsProps.LAPTOP_TYPE, state.currentLaptopsType, sortedProducts, state.customControlsArrays.laptopsTypes, curHistoryState, () => sortedProducts.filter(item => item.laptop_type === state.currentLaptopsType)).historyValue;
  }
  if (state.currentLaptopsRAM !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.RAM, state.currentLaptopsRAM, sortedProducts, state.customControlsArrays.laptopsRAMs, curHistoryState, () => sortedProducts.filter(item => +(item.ram) >= +(state.currentLaptopsRAM))).generalArray;
    curHistoryState = customSort(appData.productsProps.RAM, state.currentLaptopsRAM, sortedProducts, state.customControlsArrays.laptopsRAMs, curHistoryState, () => sortedProducts.filter(item => +(item.ram) >= +(state.currentLaptopsRAM))).historyValue;
  }
  if (state.currentLaptopsScreen !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.SCREEN, state.currentLaptopsScreen, sortedProducts, state.customControlsArrays.laptopsScreens, curHistoryState, () => sortedProducts.filter(item => +(item.screen) >= +(state.currentLaptopsScreen))).generalArray;
    curHistoryState = customSort(appData.productsProps.SCREEN, state.currentLaptopsScreen, sortedProducts, state.customControlsArrays.laptopsScreens, curHistoryState, () => sortedProducts.filter(item => +(item.screen) >= +(state.currentLaptopsScreen))).historyValue;
  }
  if (state.currentLaptopsCPU !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.PROCESSOR, state.currentLaptopsCPU, sortedProducts, state.customControlsArrays.laptopsCPUs, curHistoryState, () => sortedProducts.filter(item => item.processor === state.currentLaptopsCPU)).generalArray;
    curHistoryState = customSort(appData.productsProps.PROCESSOR, state.currentLaptopsCPU, sortedProducts, state.customControlsArrays.laptopsCPUs, curHistoryState, () => sortedProducts.filter(item => item.processor === state.currentLaptopsCPU)).historyValue;
  }

  // cameras
  if (state.currentCamerasType !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.CAMERA_TYPE, state.currentCamerasType, sortedProducts, state.customControlsArrays.camerasTypes, curHistoryState, () => sortedProducts.filter(item => item.camera_type === state.currentCamerasType)).generalArray;
    curHistoryState = customSort(appData.productsProps.CAMERA_TYPE, state.currentCamerasType, sortedProducts, state.customControlsArrays.camerasTypes, curHistoryState, () => sortedProducts.filter(item => item.camera_type === state.currentCamerasType)).historyValue;
  }

  if (state.currentCamerasMatrix !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.MATRIX_RESOLUTION, state.currentCamerasMatrix, sortedProducts, state.customControlsArrays.camerasMatrix, curHistoryState, () => sortedProducts.filter(item => +(item.matrix_resolution) >= +(state.currentCamerasMatrix))).generalArray;
    curHistoryState = customSort(appData.productsProps.MATRIX_RESOLUTION, state.currentCamerasMatrix, sortedProducts, state.customControlsArrays.camerasMatrix, curHistoryState, () => sortedProducts.filter(item => +(item.matrix_resolution) >= +(state.currentCamerasMatrix))).historyValue;
  }

  if (state.currentCamerasVideo !== appData.DEFAULT_VALUE) {
    sortedProducts = customSort(appData.productsProps.VIDEO_RESOLUTION, state.currentCamerasVideo, sortedProducts, state.customControlsArrays.camerasVideo, curHistoryState, () => sortedProducts.filter(item => item.video_resolution >= state.currentCamerasVideo)).generalArray;
    curHistoryState = customSort(appData.productsProps.VIDEO_RESOLUTION, state.currentCamerasVideo, sortedProducts, state.customControlsArrays.camerasVideo, curHistoryState, () => sortedProducts.filter(item => item.video_resolution >= state.currentCamerasVideo)).historyValue;
  }

  sortedProducts.forEach((product) => {
    if (product.price === 0) {
      productsWithZeroPrice.push(product);
    }
  });

  sortedProducts = sortedProducts.filter(item => item.price !== 0);

  const prices = getArrayKeysFromArrayObjects(sortedProducts, appData.productsProps.PRICE);

  if (sortedProducts.length <= 0) {
    placeholderVisible = true;
    curHistoryState = curHistoryState.concat('&no-products');
  }

  // range
  if (state.rangeCurrentNum !== appData.DEFAULT_VALUE) {
    sortedProducts = sortedProducts.filter(item => item.price <= state.rangeCurrentNum);
  }

  sortedProducts = [...sortedProducts, ...productsWithZeroPrice];

  rangeMin = prices.sort(sortMin)[0];
  rangeMax = prices.sort(sortMin)[prices.length - 1];
  curHistoryState = `${state.category}&${curHistoryState}&range${rangeMin}-${rangeCurrentNum}`;
  window.history.replaceState(null, null, curHistoryState);
  return updateObject(state, {
    screenProducts: sortedProducts,
    currentProducts: sortedProducts,
    rangeMin,
    rangeMax,
    rangeCurrentNum,
    placeholderVisible,
    currentHistoryState: curHistoryState
  });
};

const saveToLocalStorage = (state, action) => {
  let favoriteProducts = JSON.parse(localStorage.getItem(appData.localStorage.PRODUCTS)) || [];
  let newFavoriteProducts;
  let check = false;

  for (const prop in favoriteProducts) {
    for (const key in favoriteProducts[prop]) {
      if (key === appData.productsProps.ID) {
        if (favoriteProducts[prop][key] === action.e.target.id) {
          favoriteProducts = favoriteProducts.filter(item => item.id !== action.e.target.id);
          check = true;
        }
      }
    }
  }
  if (check === false) {
    newFavoriteProducts = state.allProducts.filter(item => item.id === action.e.target.id);
    favoriteProducts = [...favoriteProducts, newFavoriteProducts[0]];
  }

  localStorage.removeItem(appData.localStorage.PRODUCTS);

  localStorage.setItem(appData.localStorage.PRODUCTS, JSON.stringify(favoriteProducts));

  return updateObject(state, {
    favoriteProducts
  });
};

const sortFavorite = (state, action) => {
  let checkedFavorite;
  let favProducts;
  let placeholderVisible;
  let controlsState = false;

  state.checkedFavorite ? checkedFavorite = false : checkedFavorite = true;
  checkedFavorite ? favProducts = state.favoriteProducts : favProducts = state.currentProducts;
  checkedFavorite ? controlsState = true : controlsState = false;
  window.history.replaceState(null, null, checkedFavorite ? 'favorite' : state.currentHistoryState);

  favProducts.length <= 0 ? placeholderVisible = true : placeholderVisible = false;

  return updateObject(state, {
    checkedFavorite,
    screenProducts: favProducts,
    isGeneralSelectDisabled: controlsState,
    isRadioButtonsDisabled: controlsState,
    isRangeDisabled: controlsState,
    isFilterButtonDisabled: controlsState,
    placeholderVisible
  });
};

const setMaxPrice = (state, action) => {
  return updateObject(state, {
    rangeCurrentNum: action.e.target.value,
  });
};

const showPopup = (state, action) => {
  const curHistoryState = state.currentHistoryState;
  window.history.replaceState(null, null, `${curHistoryState}$product-info-${action.info.id}`);
  return updateObject(state, {
    isOverlayShow: true,
    productsInfo: action.info,
    currentHistoryState: curHistoryState
  });
};

const closePopup = (state, action) => {
  const curHistoryState = state.currentHistoryState;
  window.history.replaceState(null, null, curHistoryState);
  return updateObject(state, {
    isOverlayShow: false,
    currentHistoryState: curHistoryState
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_PRODUCTS: return initProducts(state, action);
    case actionTypes.GET_SELLERS: return initSellers(state, action);
    case actionTypes.FILTER_CATS: return setCategory(state, action);
    case actionTypes.SORT_STANDART: return sortProductsCost(state, action);
    case actionTypes.FILTER_AUTO_YEAR: return filterAutoYear(state, action);
    case actionTypes.FILTER_AUTO_TRANSMISSION: return filterAutoTransmission(state, action);
    case actionTypes.FILTER_AUTO_BODY_TYPE: return filterAutoBodyType(state, action);
    case actionTypes.FILTER_BUTTON_CLICK: return filterButtonClick(state, action);
    case actionTypes.FILTER_IMMOVABLE_TYPE: return filterImmovableType(state, action);
    case actionTypes.FILTER_IMMOVABLE_AREA: return filterImmovableArea(state, action);
    case actionTypes.FILTER_IMMOVABLE_ROOMS: return filterImmovableRooms(state, action);
    case actionTypes.FILTER_LAPTOPS_TYPES: return filterLaptopsTypes(state, action);
    case actionTypes.FILTER_LAPTOPS_RAMS: return filterLaptopsRAMs(state, action);
    case actionTypes.FILTER_LAPTOPS_SCREENS: return filterLaptopsScreens(state, action);
    case actionTypes.FILTER_LAPTOPS_CPUS: return filterLaptopsCPUs(state, action);
    case actionTypes.FILTER_CAMERAS_TYPES: return filterCamerasTypes(state, action);
    case actionTypes.FILTER_CAMERAS_MATRIX: return filterCamerasMatrix(state, action);
    case actionTypes.FILTER_CAMERAS_VIDEO: return filterCamerasVideo(state, action);
    case actionTypes.SAVE_TO_LOCAL: return saveToLocalStorage(state, action);
    case actionTypes.SORT_FAV: return sortFavorite(state, action);
    case actionTypes.SET_MAX_PRICE: return setMaxPrice(state, action);
    case actionTypes.SHOW_POPUP: return showPopup(state, action);
    case actionTypes.CLOSE_POPUP: return closePopup(state, action);
    default: return state;
  }
};

export default reducer;
