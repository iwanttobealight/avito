export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  };
};

export const sortMin = (first, second) => {
  if (+(first) < +(second)) {
    return -1;
  } else if (+(first) > +(second)) {
    return 1;
  } else {
    return 0;
  }
};

export const sortMax = (first, second) => {
  if (+(first) > +(second)) {
    return -1;
  } else if (+(first) < +(second)) {
    return 1;
  } else {
    return 0;
  }
};

export const retainUniqueElements = arr => {
  const obj = {};

  for (let i = 0; i < arr.length; i++) {
    const str = arr[i];
    obj[str] = true;
  }

  return Object.keys(obj);
};

export const retainUniqueElementsFromObjectsArray = arr => arr.filter((elem, pos) => arr.indexOf(elem) === pos);

export const getArrayKeysFromArrayObjects = (arrayObjects, keyName) => {
  const result = [];
  for (const prop in arrayObjects) {
    for (const key in arrayObjects[prop]) {
      if (key === keyName) {
        result.push(arrayObjects[prop][key]);
      }
    }
  }
  return result;
};

export const formatPrice = (value) => {
  const price = String(value).split('');
  if (price.length >= 5) {
    for (let i = price.length; i > 0; i -= 3) {
      if (price[i]) {
        price.splice(i, 0, ' ');
      }
    }
  }
  return price;
};

export const customSort = (label, currentValue, generalArray, customArray, historyValue, filterArray) => {
  if (currentValue === 'all') {
    generalArray = customArray;
    historyValue = historyValue.concat(`&${label}-all`);
  } else {
    generalArray = filterArray();
    if (generalArray[generalArray.length - 1]) {
      historyValue = historyValue.concat(`&${label}-${generalArray[generalArray.length - 1][label]}`);
    }
  }
  return { generalArray, historyValue };
};

